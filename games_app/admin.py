from django.contrib import admin

from games_app.models import Creator, Demo, GameReview

# Register your models here.

admin.site.register(Demo)
admin.site.register(Creator)
admin.site.register(GameReview)
