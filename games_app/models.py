from django.db import models
from django.contrib.auth.models import User

class Creator(models.Model):
    name = models.CharField(max_length= 50, unique=True)

    def __str__(self):
        return self.name


class Demo(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(max_length= 500)
    date_published = models.DateField()
    creator = models.ManyToManyField(Creator, related_name="creator")
    image = models.ImageField(upload_to="images/", blank=True, null=True)
    game_file = models.FileField(upload_to="games/", blank = True, null = True)

    def __str__(self):
        return self.name


class GameReview(models.Model):
    reviwer = models.ForeignKey(User, related_name="user_reviews", on_delete=models.CASCADE)
    game = models.ForeignKey(Demo, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return f"Review for: {self.game} | by: {self.reviwer}"



